FROM python:3.10-alpine

## update pip and install requeriments libraries
RUN pip install --upgrade pip
COPY ./requeriments.txt .
RUN pip install -r requeriments.txt

## create app folder and copy my project inside 
COPY ./django_gitlab /app
WORKDIR /app

COPY ./entrypoint.sh /
ENTRYPOINT [ "sh", "entrypoint.sh" ]

